# SparkReadClust (SparRC): a scalable tool for sequence reads clustering using Apache Spark

## Summary:

SpaRC is an Apache Spark-based scalable read clustering application that partitions the sequence reads (transcriptomics or metagenomics) based on their molecule of origin to enable downstream analysis in parallel (e.g., de novo assembly). 
 
To cite SpaRC: 
Lizhen Shi, Xiandong Meng, Elizabeth Tseng, Michael Mascagni, Zhong Wang; 
SpaRC: Scalable Sequence Clustering using Apache Spark, Bioinformatics, , bty733, 
https://doi.org/10.1093/bioinformatics/bty733

## Requirements:

The development environment uses the following settings.

1. ``>=``Spark 2.0.1
2. ``>=``Scala 2.11.8
3. ``>=``Hadoop 2.7.3
4. ``>=``JAVA 1.8

Please edit/use a specific version of build.sbt according to your version of Spark and related software. 

## Build (Local):

0. install java, sbt, spark and hadoop if necessary, and make sure environment variables of JAVA_HOME, HADOOP_HOME, SPARK_HOME are set.
1. optionally use "sbt -mem 4096 test" to run unit tests (this example uses 4G memory).
2. run "sbt assembly" to make the assembly jar.

## Build (AWS EMR):

0. install git, clone the repo on the master node.
1. download sbt and extract:

        wget https://github.com/sbt/sbt/releases/download/v1.2.3/sbt-1.2.3.tgz
        tar xzf sbt-1.2.3.tgz 
    
2. edit build.sbt if necessary to the corresponding versions of Spark/Hadoop, etc
3. run "～/sbt/bin/sbt assembly" command in sparRC directory   
   
## Input Formats
### Fasta
the FASTA format is a text-based format for representing either nucleotide sequences or amino acid (protein) sequences.An example is
>seq0
FQTWEEFSRAAEKLYLADPMKVRVVLKYRHVDGNLCIKVTDDLVCLVYRTDQAQDVKKIEKF
>seq1
KYRTWEEFTRAAEKLYQADPMKVRVVLKYRHCDGNLCIKVTDDVVCLLYRTDQAQDVKKIEKFHSQLMRLME LKVTDNKECLKFKTDQAQEAKKMEKLNNIFFTLM
### Fastq
@S15R0/1
TCATCACAAGCAGTCAAACCTGCCGACAAACAAAGGAGAAGACATGAATATCTCATCAAATGATTCATCATCTTTATCTACTTTTTAATACTGGCATATCCAAACATGCCAAAGAAGTAAAGGTAGGATTCTCCAAATCCCCCCCTTTAC
+
DADGGGGGGIIHIJJFKJKKGKKCEI<GJKK7KIIKKKH<IKGKFJAKCKKEFIEG$EJE=HEJ>JEJGGKCFDB>IC3@?EEEEF=EDE$EEECEDEE?EEDDEEEEEEEFDCDFBEDEDD=$$DECA?DEEEBDCE@AD;A$DE$ECA
@S15R0/2
ATTTGGAATGGAGCGAGGTTGATGATGATGCAAAAGTTGAATCTGCCCAAAATGTACGCGGATACCATACTATAGAATTCCTGCTTTTCAAGGATGTCAAGCCTCGTACAGTTAAATAAGAATTAATTATAAACACGAGTCATGAAGTAA
+
D<9GC>FGIIB3IKK3JJ$KKJKHKKEJJKAGKC?KEIKK?KKKJJKACJK9EKEKGEE>BFD$IEK$JFF$EEFD?EEDCEEEDEEFC68EEEEE$EEFBB$EEE@EBEAE?EAEDEEE$EDE?$A?EF?$=?G$$6$B$=$:$CFEAB
### Seq

A seq file contains lines of ID, HEAD, SEQ which are separated by TAB ("\t"). An example is

    1	k103_14993672 flag=1 multi=14.0000 len=10505	TCTAAGTAACATTGACACGTAGCACACATAG
    2	k103_13439317 flag=0 multi=59.2840 len=4715	CGCAGTTATCTTTTTCAAATTCTGGCCGATAA

Fasta or fastq files can be converted to .seq file using the provided scripts:

    fastaToSeq.py
    fastqToSeq.py

for contigs files that have been sorted by sequence length, please enable "shuffle" function to balance data partitions
for fasta files with sequence span multiple lines (formatted), using the following command:

    awk '!/^>/ { printf "%s", $0; n = "\n" } /^>/ { print n $0; n = "" } END { printf "%s", n }' multi.fa > singleline.fa

For seq file of old version, the numeric ID may be missing, use the script to add IDs, e.g.

     cat *.seq | scripts/seq_add_id.py > new_seq.txt

### Parquet

_Parquet_ file is a binary compressed format file. It can be converted from _Seq_ file by

    spark-submit --master local[4] --driver-memory 16G --class org.jgi.spark.localcluster.tools.Seq2Parquet target/scala-2.11/LocalCluster-assembly-0.1.jar -i test/small/*.seq -o tmp/parquet_output

Help

    $ spark-submit --master local[4] --driver-memory 16G --class org.jgi.spark.localcluster.tools.Seq2Parquet target/scala-2.11/LocalCluster-assembly-*.jar --help
    
    Seq2Parquet 0.1
    Usage: Seq2Parquet [options]
    
      -i, --input <dir>        a local dir where seq files are located in,  or a local file, or an hdfs file
      -p, --pattern <pattern>  if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq
      -o, --output <dir>       output of the top k-mers
      --coalesce               coalesce the output
      -n, --n_partition <value>
                               paritions for the input, only applicable to local files
      --help                   prints this usage text

### Base64

Although Parquet has advantages of data schema and compression etc, use base64 encoding is simpler. Its text format is more flexible and the compression is better.
Convert seq file to base64 format use

    spark-submit --master local[4] --driver-memory 16G --class org.jgi.spark.localcluster.tools.Seq2Base64 target/scala-2.11/LocalCluster-assembly-0.1.jar -i test/small/*.seq -o tmp/base64_output --coalesce

Help

     $ spark-submit --master local[4] --driver-memory 16G --class org.jgi.spark.localcluster.tools.Seq2Base64 target/scala-2.11/LocalCluster-assembly-0.1.jar --help
    
    Seq2Base64 0.1
    Usage: Seq2Base64 [options]
    
      -i, --input <dir>        a local dir where seq files are located in,  or a local file, or an hdfs file
      -p, --pattern <pattern>  if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq
      -o, --output <dir>       output of the top k-mers
      --coalesce               coalesce the output
      -n, --n_partition <value>
                               paritions for the input, only applicable to local files
      --help                   prints this usage text

## Calculate read overlaps using Kmer/Mmer

SpaRC uses either Kmers or Minimizers (Mmer) to apporximate read overlaps. Mmer can greatly reduce the memory usage and compute time. Warning: Mmer is not recommended for reads with high error rate (uncorrected PacBio or Nanopore reads).
Example for seq files

Kmer:

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.executor.extraClassPath=target/scala-2.11/LocalCluster-assembly-0.1.jar --conf spark.driver.maxResultSize=8g --conf spark.network.timeout=360000 --conf spark.speculation=true --conf spark.default.parallelism=25000 --conf spark.eventLog.enabled=false --conf spark.executor.userClassPathFirst=true --conf spark.driver.userClassPathFirst=true target/scala-2.11/LocalCluster-assembly-0.1.jar KmerMapReads2 --wait 1 --reads data/maize14G.seq --format seq -o tmp/maize14G_kmerreads.txt_31 -k 31 --kmer tmp/maize14G_kc_seq_31 --contamination 0 --min_kmer_count 2 --max_kmer_count 100000 -C --n_iteration 1

Mmer:

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.executor.extraClassPath=target/scala-2.11/LocalCluster-assembly-0.1.jar --conf spark.driver.maxResultSize=8g --conf spark.network.timeout=360000 --conf spark.speculation=true --conf spark.default.parallelism=25000 --conf spark.eventLog.enabled=false --conf spark.executor.userClassPathFirst=true --conf spark.driver.userClassPathFirst=true target/scala-2.11/LocalCluster-assembly-0.1.jar MinimizerMapReads2 --wait 1 --reads data/maize14G.seq --format seq -o tmp/maize14G_kmerreads.txt_31 -k 21 -w 20 --kmer tmp/maize14G_kc_seq_31 --contamination 0 --min_kmer_count 2 --max_kmer_count 100000 -C --n_iteration 1


Also use "--help" to see the usage

    KmerMapReads 0.5 (MinimizerMapReads)
    Usage: KmerMapReads [options]
    
      --reads <dir/file>       a local dir where seq files are located in,  or a local file, or an hdfs file
      -p, --pattern <pattern>  if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq
      -o, --output <dir>       output of the top k-mers
      --format <format>        input format (seq, parquet or base64)
      -C, --canonical_kmer     apply canonical kmer
      -c, --contamination <value>
                               the fraction of top k-mers to keep, others are removed likely due to contamination
      --wait <value>           wait $slep second before stop spark session. For debug purpose, default 0.
      -n, --n_partition <value>
                               paritions for the input, only applicable to local files
      -k, --kmer_length <value>
                               length of k-mer (m-mer)
      -w, --kmer_length <value>
                               length of window when using minimizers                               
      --min_kmer_count <value>
                               minimum number of reads that shares a kmer
      --max_kmer_count <value>
                               maximum number of reads that shares a kmer. greater than max_kmer_count, however don't be too big
      --n_iteration <value>    #iterations to finish the task. default 1. set a bigger value if resource is low.
      --help                   prints this usage text



## Generate graph edges
Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.executor.extraClassPath=target/scala-2.11/LocalCluster-assembly-0.1.jar --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.speculation=true --conf spark.default.parallelism=5000 --conf spark.eventLog.enabled=true target/scala-2.11/LocalCluster-assembly-0.1.jar GraphGen2 --wait 1 -i tmp/maize14G_kmerreads.txt_31 -o tmp/maize14G_edges.txt_31 --min_shared_kmers 10 --max_shared_kmers 20000 --max_degree 50

Also use "--help" to see the usage

    GraphGen 0.5
    Usage: GraphGen [options]
    
      -i, --kmer_reads <file>  reads that a kmer shares. e.g. output from KmerMapReads
      -o, --output <dir>       output of the top k-mers
      --wait <value>           wait $slep second before stop spark session. For debug purpose, default 0.
      --max_degree <value>     max_degree of a node
      --min_shared_kmers <value>
                               minimum number of kmers that two reads share
      -n, --n_partition <value>
                               paritions for the input
      --n_iteration <value>    #iterations to finish the task. default 1. set a bigger value if resource is low.
      --help                   prints this usage text




## Find clusters by connected components
Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.default.parallelism=5000 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true target/scala-2.11/LocalCluster-assembly-0.1.jar GraphCC --wait 1 -i tmp/maize14G_edges.txt -o tmp/maize14G_cc.txt --min_shared_kmers 10 --max_shared_kmers 20000 --min_reads_per_cluster 2 --n_iteration 1 --use_graphframes --top_nodes_ratio 0.05 --big_cluster_threshold 0.05


Also use "--help" to see the usage

    GraphCC 0.5
    Usage: GraphCC [options]
    
      -i, --edge_file <file>   files of graph edges. e.g. output from GraphGen
      -o, --output <dir>       output file
      -n, --n_partition <value>
                               paritions for the input
      --top_nodes_ratio <value>
                               within a big cluster, top-degree nodes will be removed to re-cluster. This ratio determines how many nodes are removed.
      --big_cluster_threshold <value>
                               Define big cluster. A cluster whose size > #reads*big_cluster_threshold is big
      --wait <value>           wait $slep second before stop spark session. For debug purpose, default 0.
      --n_output_blocks <value>
                               output block number
      --use_graphframes
      --min_shared_kmers <value>
                               minimum number of kmers that two reads share
      --max_shared_kmers <value>
                               max number of kmers that two reads share
      --n_iteration <value>    #iterations to finish the task. default 1. set a bigger value if resource is low.
      --min_reads_per_cluster <value>
                               minimum reads per cluster
      --help                   prints this usage text


## Or find clusters by Label Propagation Algorithm
Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.default.parallelism=5000 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GraphLPA2 target/scala-2.11/LocalCluster-assembly-0.1.jar --wait 1 -i tmp/maize14G_edges.txt_31 -o tmp/maize14G_lpa.txt_31 --min_shared_kmers 10 --max_shared_kmers 20000 --min_reads_per_cluster 2 --max_iteration 50


Also use "--help" to see the usage

    GraphLPA 0.5
    Usage: GraphLPA [options]
    
      -i, --edge_file <file>   files of graph edges. e.g. output from GraphGen
      -o, --output <dir>       output file
      -n, --n_partition <value>
                               paritions for the input
      --max_iteration <value>  max ietration for LPA
      --wait <value>           wait $slep second before stop spark session. For debug purpose, default 0.
      --n_output_blocks <value>
                               output block number
      --min_shared_kmers <value>
                               minimum number of kmers that two reads share
      --max_shared_kmers <value>
                               max number of kmers that two reads share
      --min_reads_per_cluster <value>
                               minimum reads per cluster
      --help                   prints this usage text


## Generate output
Save the output in text format ("seq-id,cluster-id" per line). Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 20G --executor-cores 2 --conf spark.default.parallelism=54 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.speculation=true --conf spark.eventLog.enabled=false target/scala-2.11/LocalCluster-assembly-0.1.jar CCAddSeq --wait 1 -i tmp/maize14G_lpa.txt_31 --reads data/maize14G.seq -o tmp/maize14G_lpaseq.txt_31

Also use "--help" to see the usage

    AddSeq 0.5
    Usage: AddSeq [options]
    
      -i, --cc_file <file>     files of graph edges. e.g. output from GraphCC
      --reads <dir|file>       a local dir where seq files are located in,  or a local file, or an hdfs file
      -p, --pattern <pattern>  if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq
      --format <format>        input format (seq, parquet or base64)
      -o, --output <dir>       output file
      -n, --n_partition <value>
                               paritions for the input
      --wait <value>           waiting seconds before stop spark session. For debug purpose, default 0.
      --help                   prints this usage text

## GlobalClustering
Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.default.parallelism=5000 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GlobalClustering target/scala-2.11/LocalCluster-assembly-0.1.jar GlobalClustering --mini_file data/Global_minimizer --lpa_file data/GraphLPA3_output  -o data/Global_output 
--jars 9,13,16,18,20,28,32,34,38,41 --kmerCount 10 --filter 0.2  --n_partition 1 --n_block 1 --wait 0 

Also use "--help" to see the usage

    GlobalClustering 0.5
    Usage: GlobalClustering [options]
    
      --lpa_file <file>        files of global clustering. e.g. output from LPA
      --mini_file <dir>        a local dir/file where seq files are located in. e.g. output from MinimizerMapReads
      -o, --output <dir>       output file
      --filter <value>         the fraction of top k-mers to keep, others are removed likely due to contamination
      --kmercount              the number of samples for input
      -n, --n_partition <value> 
                               paritions for the input, only applicable to local files
      -p, --pattern<value> 
                               if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq
      --jars <value>           jars to include(This is a bug,may be need to know the sample in detail.) e.g. 1,2,3,4,5
      --n_block <value> 
                               output block number
      --wait <value>           waiting seconds before stop spark session. For debug purpose, default 0.
      --help                   prints this usage text

## Find global clusters by Label Propagation Algorithm
Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.default.parallelism=5000 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GraphLPA2 target/scala-2.11/LocalCluster-assembly-0.1.jar --wait 1 -i tmp/maize14G_edges.txt_31 -o tmp/maize14G_lpa.txt_31 --min_shared_kmers 10 --max_shared_kmers 20000 --min_reads_per_cluster 2 --max_iteration 50


Also use "--help" to see the usage

    GraphLPA 0.5
    Usage: GraphLPA [options]
    
      -i, --edge_file <file>   files of graph edges. e.g. output from GraphGen
      -o, --output <dir>       output file
      -n, --n_partition <value>
                               paritions for the input
      --max_iteration <value>  max ietration for LPA
      --wait <value>           wait $slep second before stop spark session. For debug purpose, default 0.
      --n_output_blocks <value>
                               output block number
      --min_shared_kmers <value>
                               minimum number of kmers that two reads share
      --max_shared_kmers <value>
                               max number of kmers that two reads share
      --min_reads_per_cluster <value>
                               minimum reads per cluster
      --help                   prints this usage text

## Generate output
Save the output in text format ("seq-id,cluster-id" per line). Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 20G --executor-cores 2 --conf spark.default.parallelism=54 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.speculation=true --conf spark.eventLog.enabled=false target/scala-2.11/LocalCluster-assembly-0.1.jar CCAddSeq --wait 1 -i tmp/maize14G_lpa.txt_31 --reads data/maize14G.seq --local_file data/local_text.txt --flag globalclustering -o tmp/maize14G_lpaseq.txt_31

Also use "--help" to see the usage

    CCAddSeq 0.5
    Usage: CCAddSeq [options]
    
      -i, --input <file>       files of graph edges. e.g. output from GraphCC
      --reads_input <dir|file>
                               a local dir where seq files are located in,  or a local file, or an hdfs file
      --local_lpa <file>       files of Label Propagation Algorithm. e.g. output from the local LPA
      -o, --output <dir>       output file
      --format <format>        input format (seq, parquet or base64)
      -n, --n_partition <value>
                               paritions for the input
      -p, --pattern <pattern>  if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq
      --flag <string>          should be one of <local|global>
      --num_output             number of paritions for the output
      --wait <value>           waiting seconds before stop spark session. For debug purpose, default 0.
      --help                   prints this usage text

## Metric

__(If your reads have known labels, you can use the following example to evaluate clustering results)__

Example

    spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G --executor-cores 2 --conf spark.default.parallelism=5000 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false --conf spark.speculation=true --class org.jgi.spark.localcluster.tools.GlobalClustering target/scala-2.11/LocalCluster-assembly-0.1.jar Metric -k data/key_test.txt -l data/lpa_test.txt -o tmp/out --flag mock -n 2 

Also use "--help" to see the usage

    Metric 0.5
    Usage: Metric [options]
    
      -k, --key_input <file>   reads from answer
      -l, --lpa_input <file>
                               reads from result.  e.g. output from lpa
      -o, --output <file>      output file
      -n, --n_partition <value> 
                               paritions for the input, only applicable to local files
      --flag <string>          should be one of <local|global>
      --wait <value>           waiting seconds before stop spark session. For debug purpose, default 0
      --help                   prints this usage text

## License

SpaRC Copyright (c) 2018, The Regents of the University of California, through Lawrence Berkeley National Laboratory (subject to receipt of any required approvals from the U.S. Dept. of Energy).  All rights reserved.

 

If you have questions about your rights to use or distribute this software, please contact Berkeley Lab's Innovation & Partnerships Office at  IPO@lbl.gov referring to " Scalable Tool for Sequence Clustering Using Apache Spark" (LBNL Ref 2017-194)."

 

NOTICE.  This software was developed under funding from the U.S. Department of Energy.  As such, the U.S. Government has been granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, prepare derivative works, and perform publicly and display publicly.  The U.S. Government is granted for itself and others acting on its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software to reproduce, prepare derivative works, distribute copies to the public, perform publicly and display publicly, and to permit others to do so.
