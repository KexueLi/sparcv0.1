package org.jgi.spark.localcluster

import java.io.PrintWriter
import java.io.FileWriter

import org.jgi.spark.localcluster.tools.MinimizerMapReads.Config


object MyMinimizer {

  private def canonical_kmer(seq: String) = {
    val rc = DNASeq.reverse_complement(seq)
    if (rc < seq) rc else seq
  }

  def generate_minimizer(config: Config,id:(Long,String),kmer: String):String= {
    // generate minimizer from a k-mer
    val m=config.m
    val krc = DNASeq.reverse_complement(kmer)
    val minimizers = (
      (0 until (kmer.length - m + 1)).map(i => kmer.substring(i,i+m))
        ++ (0 until (krc.length - m + 1)).map(i => krc.substring(i,i+m))
      ).sorted
    val mn=minimizers(0)
    //println("the minimizer  "+mn)
    mn
  }


  def generate_kmers(config: Config,id:(Long,String),seq: String, k: Int, m: Int): Array[DNASeq] = {
    seq.split("R|Y|M|K|S|W|H|B|V|D|Y|N").flatMap {
      subSeq => {
        (0 until (subSeq.length - k + 1)).map {
          i =>
            val kmer = subSeq.substring(i, i + k)
            val a = DNASeq.from_bases(kmer)
            a
        }
      }.distinct
    }
  }

  def main(args: Array[String]) ={

    /* val k=31
     val m=15

     val seq="ATCGGACTTTTACGATACGGAAAGTTCGTTCAGAACCGTTACCAGATTAGGAAACCCATGTACAAAGTCAGTAAGTCAAGTCCAGTCCCAAAAGTCCCAA"
     println(seq.length)
     val aaa=generate_mmers(seq, k, m)
     for(i<-0 until aaa.length){
       println(aaa(i))
     }
     val bbb=aaa.map{i=>i.to_base64}

     println("the result is"+bbb.mkString)
     println(bbb.length)*/
  }

}

