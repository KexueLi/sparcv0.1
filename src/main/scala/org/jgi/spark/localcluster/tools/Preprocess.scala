package org.jgi.spark.localcluster.tools

/**
  * Created by Lizhen Shi on 6/8/17.
  */

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.jgi.spark.localcluster.{DNASeq, Utils}
import sext._


object Preprocess extends App with LazyLogging {

  case class Config(output: String = "", input: Seq[String] = Seq(),n_partition: Int = -1,n_output_blocks: Int = 100,
                    format:String = "", paired:String="true", shuffle: String = "false")

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("SeqAddId") {
      head("Transformat", Utils.VERSION)
      opt[Seq[String]]('i',"input").valueName("<sample1.seq>,<sample2.seq>...").action( (x,c) =>
        c.copy(input = x) ).text("sample input")
      opt[String]('o', "output").required().valueName("<dir>").action((x, c) =>
        c.copy(output = x)).text("output file")
      opt[Int]('n', "n_partition").action((x, c) =>
        c.copy(n_partition = x))
        .text("paritions of input")
      opt[Int]( "n_output_blocks").action((x, c) =>
        c.copy(n_output_blocks = x))
        .text("#blocks for output")

      opt[String]("format").valueName("<string>").action((x, c) =>
        c.copy(format = x)).validate(x =>
        if (Seq("fasta","fastq","seq").contains(x.toLowerCase)) success
        else failure("should be one of <fasta|fastq|seq>"))
        .text("input format:<fasta|fastq|seq>")
      opt[String]( "paired").required().valueName("<Boolean>").action((x, c) =>
        c.copy(paired = x)).text("<true|false>")
      opt[String]( "shuffle").optional().valueName("<Boolean>").action((x, c) =>
        c.copy(shuffle = x)).text("<true|false>")
      help("help").text("prints this usage text")
    }
    parser.parse(args, Config())
  }
  def seqAddID(config: Config, sc: SparkContext,allRDD:RDD[(Int,Long,String)],value:Int): RDD[String] ={
    logger.info(s"seqAddID is running...")
    val addID=allRDD.zipWithIndex().map{case((sampleID, index,seq),newindex)=>
      newindex.toString+"\t"+sampleID.toString+"\t"+seq}
    addID
  }

  def fastAddID( config: Config, sc: SparkContext,allRDD:RDD[(Int,Long,String)],value:Int):RDD[String] ={
    logger.info(s"${config.format}AddID(paired=${config.paired}) is running...")
    val addID=
      if(config.paired=="true") {
        val PairAddID= allRDD.map{
          case(sampleID, index,str)=>
            ((sampleID,index/value),str,index%value)//group using / and % information
        }.filter(x=>x._3==0|x._3==1|x._3==value/2+1)
          .groupBy(_._1)
          .filter(_._2.toSeq.length==3)
          .zipWithIndex().map{
          x=>
            x._2.toString+"\t"+x._1._1._1+"\t"+x._1._2.toSeq(0)._2+"\t"+
              x._1._2.toSeq(1)._2+"N"+x._1._2.toSeq(2)._2 //readsID sampleID readsHead seq
        }
        PairAddID
      }else if(config.paired=="false"){
        val RawAddID= allRDD.map{
          case(sampleID, index,str)=>
            ((sampleID,index/(value/2)),str,index%(value/2))//group using / and % information
        }.filter(x=>x._3==0|x._3==1)
          .groupBy(_._1)
          .filter(_._2.toSeq.length==2)
          .zipWithIndex().map{
          x=>
            x._2.toString+"\t"+x._1._1._1+"\t"+x._1._2.toSeq(0)._2+"\t"+ x._1._2.toSeq(1)._2
        }
        RawAddID
      }else {
        println(" bad arguments");sys.exit(-1)
      }
    addID
  }
  def run(config: Config, sc: SparkContext): Unit = {
    val start = System.currentTimeMillis
    logger.info(new java.util.Date(start) + ": Program started ...")

    val rdd = 0.until(config.input.length).map {
      i =>
        println(config.input(i))
        (if(config.n_partition>0)
          sc.textFile(config.input(i),config.n_partition)
        else  sc.textFile(config.input(i)))
          .map {
            line => line.split("\n")
          }.zipWithIndex().map(x=>(i,x._2,x._1(0)))//sampleID, index,str
    }.toSeq
    val allRDD=sc.union(rdd)
    val res=config.format match {
      case "fastq" => fastAddID(config,sc,allRDD,8)
      case "fasta" => fastAddID(config,sc,allRDD,4)
      case "seq" =>  seqAddID(config,sc,allRDD,4)
      case _ =>{println(" bad arguments");sys.exit(-1)}
    }
    res.take(5).foreach(println(_))

    KmerCounting.delete_hdfs_file(config.output)
    res.repartition(config.n_output_blocks).saveAsTextFile(config.output)
    logger.info(s"save results to ${config.output}")
    val totalTime1 = System.currentTimeMillis
    logger.info("Processing time: %.2f minutes".format((totalTime1 - start).toFloat / 60000))
  }

  override def main(args: Array[String]) {

    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")
        val conf = new SparkConf().setAppName("Spark SeqAddId")
        conf.registerKryoClasses(Array(classOf[DNASeq]))

        val sc = new SparkContext(conf)
        run(config, sc)

        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  } //main
}

