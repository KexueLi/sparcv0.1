package org.jgi.spark.localcluster.tools

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.jgi.spark.localcluster._

import java.io.PrintWriter

import scala.util.Random

object MinimizerMapReads extends App with LazyLogging {

  case class Config(flag:String="global",input: String = "",kmer:String="", minimizer: String = "", pattern: String = "",
                    n_iteration: Int = 1, k: Int = 31, m: Int = 15, min_kmer_count: Int = 1, sleep: Int = 0,
                    max_kmer_count: Int = 200, format: String = "seq", n_partition: Int = 0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("MinimerMapReads") {
      head("MinimerMapReads", Utils.VERSION)

      opt[String]("flag").valueName("").action((x, c) =>
        c.copy(flag = x)).
        validate(x =>
          if (Seq("local","global").contains(x.toLowerCase)) success
          else failure("should be one of <local|global>"))
        .text("clustering schema. <local|global>")

      opt[String]('i', "input").required().valueName("<dir/file>").action((x, c) =>
        c.copy(input = x)).text("a local dir where seq files are located in,  or a local file, or an hdfs file")

      opt[String]("kmer").required().valueName("<dir/file>").action((x, c) =>
        c.copy(kmer = x)).text("output file which the GlobalCluatering need")

      opt[String]('p', "pattern").valueName("<pattern>").action((x, c) =>
        c.copy(pattern = x)).text("if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq")

      opt[String]("minimizer").required().valueName("<dir>").action((x, c) =>
        c.copy(minimizer = x)).text("output of the top k-mers")

      opt[String]("format").valueName("<format>").action((x, c) =>
        c.copy(format = x)).
        validate(x =>
          if (List("seq", "parquet", "base64").contains(x)) success
          else failure("only valid for seq, parquet or base64")
        ).text("input format (seq, parquet or base64)")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

      opt[Int]('n', "n_partition").action((x, c) =>
        c.copy(n_partition = x))
        .text("paritions for the input, only applicable to local files")

      opt[Int]('k', "kmer_length").required().action((x, c) =>
        c.copy(k = x)).
        validate(x =>
          if (x >= 11) success
          else failure("k is too small, should not be smaller than 11"))
        .text("length of k-mer")

      opt[Int]('m', "minimizer_length").required().action((x, c) =>
        c.copy(m = x)).
        validate(x =>
          if (x >= 5) success
          else failure("k is too small, should not be smaller than 5"))
        .text("length of minimizer")

      opt[Int]("min_kmer_count").action((x, c) =>
        c.copy(min_kmer_count = x)).
        validate(x =>
          if (x >= 2) success
          else failure("min_kmer_count should be greater than 2"))
        .text("minimum number of reads that shares a kmer")

      opt[Int]("max_kmer_count").action((x, c) =>
        c.copy(max_kmer_count = x)).
        validate(x =>
          if (x >= 2) success
          else failure("max_kmer_count should be greater than 2"))
        .text("maximum number of reads that shares a kmer. greater than max_kmer_count, however don't be too big")

      opt[Int]("n_iteration").action((x, c) =>
        c.copy(n_iteration = x)).
        validate(x =>
          if (x >= 1) success
          else failure("n should be positive"))
        .text("#iterations to finish the task. default 1. set a bigger value if resource is low.")

      help("help").text("prints this usage text")

    }
    parser.parse(args, Config())
  }

  def logInfo(str: String) = {
    println(str)
    logger.info(str)
  }

  def make_read_id_rdd(file: String, format: String, sc: SparkContext): RDD[(Long, String)] = {
    if (format.equals("parquet")) throw new NotImplementedError
    else {
      var textRDD =
        sc.textFile(file)

      if (format.equals("seq")) {
        textRDD.map {
          line => line.split("\t|\n")
        }.map { x => (x(0).toLong, x(2)) }
      }

      else if (format.equals("base64")) {
        throw new IllegalArgumentException
      }
      else
        throw new IllegalArgumentException
    }

  }


  def make_reads_rdd(file: String, format: String, n_partition: Int, sc: SparkContext):RDD[((Long,String),String)]= {
    make_reads_rdd(file, format, n_partition, -1, sc)
  }

  def make_reads_rdd(file: String, format: String, n_partition: Int, sample_fraction: Double, sc: SparkContext)= {


    val textRDD= sc.textFile(file).map {
      line => line.split("\t|\n")
    }.map { x => ((x(0).toLong,x(1)), x(3)) }
    textRDD
  }

  private def process_iteration(i: Int, readsRDD: RDD[((Long, String),String)], config: Config, sc: SparkContext) = {
    println(s"Running minimizer, K: ${config.k}, M: ${config.m}")
    val kmer_gen_fun = (id:(Long,String),seq: String) =>
      MyMinimizer.generate_kmers(config: Config,id=id,seq= seq, k=config.k, m=config.m)
    val kmer = readsRDD.mapPartitions {
      iterator => iterator.map {
        case (id, seq) =>
          kmer_gen_fun(id,seq)
            .filter { x =>
              (Utils.pos_mod(x.hashCode, config.n_iteration) == i)
            }.distinct.map(s => (s,id._1,id._2))
      }
    } .flatMap(x => x).persist(StorageLevel.MEMORY_AND_DISK)

    //the GlobalClustering sample
    if(config.flag.toLowerCase=="global"){
      println("Global clustering is running...")
      KmerCounting.delete_hdfs_file(config.kmer)
      val kmerAA=kmer.filter{x=>x._1.toString.substring(0,1)=="A"}
      kmerAA.map{
        case(seq,cluster,sample)=>
          seq + " " +sample+ " " +cluster
      }.saveAsTextFile(config.kmer)
      println("The kmer is:")
      kmerAA.take(10).foreach(println(_))
    }

    val minimizer_gen_fun = (config: Config, id:(Long,String),seq: String) =>
      MyMinimizer.generate_minimizer(config: Config,id,seq)
    val minimizer=kmer.mapPartitions {
      iterator =>
        iterator.map {
          case (dnaSeq, cluster, sample) =>
            val temp=minimizer_gen_fun(config,(cluster, sample),dnaSeq.toString)
            (temp,Set(cluster))
        }
    }.reduceByKey(_ ++ _)
      .map(u => (u._1, u._2.toSeq))
      .filter(u => u._2.length >= config.min_kmer_count)
    minimizer.map {
      case (kmer, reads) =>
        (kmer, if (reads.length <= config.max_kmer_count) reads else (Random.shuffle(reads).take(config.max_kmer_count)))
    }
    KmerCounting.delete_hdfs_file(config.minimizer)
    sc.union(minimizer).map(x => x)
      //.groupBy(_._1).map(x => (x._1, x._2.flatMap(_._2)))
      .map(x =>DNASeq.from_bases(x._1).to_base64 + " " + x._2.mkString(","))
      .saveAsTextFile(config.minimizer)

    kmer.unpersist()

    logInfo(s"Generate ${minimizer.count} minimizer (#count>1 ) for iteration $i")
  }

  def run(config: Config, sc: SparkContext): Unit = {

    val start = System.currentTimeMillis
    logInfo(new java.util.Date(start) + ": Program started ...")

    val seqFiles = Utils.get_files(config.input.trim(), config.pattern.trim())
    logger.debug(seqFiles)

    val readsRDD = MinimizerMapReads.make_reads_rdd(seqFiles, config.format, config.n_partition, sc)
    readsRDD.cache()
    println("readRdd:")
    println(readsRDD.count())

    val rdds = 0.until(config.n_iteration).map {
      i =>
        process_iteration(i, readsRDD, config, sc)
    }


    readsRDD.unpersist(blocking = false)
    //Thread.sleep(360 * 1000)
    val totalTime1 = System.currentTimeMillis
    logInfo("Total process time: %.2f minutes".format((totalTime1 - start).toFloat / 60000))
  }

  override def main(args: Array[String]) {

    val options = parse_command_line(args)
    //logger.info(s"called with arguments\n${options.valueTreeString}")
    options match {
      case Some(_) =>
        val config = options.get
        if (config.input.startsWith("hdfs:") && config.n_partition > 0) {
          logger.error("do not set partition when use hdfs input. Change block size of hdfs instead")
          sys.exit(-1)
        }

        if (config.max_kmer_count < config.min_kmer_count) {
          logger.error("max_kmer_count should not be less than min_kmer_count")
          sys.exit(-1)
        }

        // logInfo(s"called with arguments\n${options.valueTreeString}")
        val conf = new SparkConf().setAppName("Spark Kmer Map Reads").set("spark.kryoserializer.buffer.max", "512m")
        conf.registerKryoClasses(Array(classOf[DNASeq]))

        val sc = new SparkContext(conf)
        run(config, sc)
        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  } //main
}
