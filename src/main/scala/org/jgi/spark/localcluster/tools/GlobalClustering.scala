/**
  * Created by Kexue Li on 2018/11/27 .
  *
  *Global Clustering
  *
  */
package org.jgi.spark.localcluster.tools

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.jgi.spark.localcluster._
import sext._
import org.apache.spark.mllib.linalg._

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import org.apache.spark.sql.SparkSession
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.distributed.{MatrixEntry, RowMatrix}


import scala.util.control.Breaks
import util.control.Breaks._
import breeze.linalg.norm
import breeze.numerics._
import shapeless.ops.tuple.Filter

import scala.math

object GlobalClustering extends App with LazyLogging {
  case class Config(mini_input:String="",lpa_input:String="", samples: Int = 1,
                    size:Int=100,kmercount:Int=30,filter: Double = 0.00005,output:String="",
                    n_partition: Int = 10,n_block:Int=1, sleep: Int = 0,pattern: String = "")

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("GlobalClustering") {
      head("GlobalClustering", Utils.VERSION)

      opt[String]("lpa_input").required().valueName("<dir/file>").action((x, c) =>
        c.copy(lpa_input= x)).text("")

      opt[String]("mini_input").required().valueName("<dir/file>").action((x, c) =>
        c.copy(mini_input = x)).text("")

      opt[String]('o',"output").required().valueName("<dir/file>").action((x, c) =>
        c.copy(output = x)).text("")

      opt[Int]("samples").valueName("Int").action( (x,c) =>
        c.copy(samples = x) ).text("#number samples")

      opt[Int]("size").required().valueName("<Int>").action((x, c) =>
        c.copy(size = x)).text("cluster size threshold")

      opt[Int]("kmercount").required().valueName("<dir/file>").action((x, c) =>
        c.copy(kmercount = x)).text("the number of samples for input")

      opt[Double]("filter").action((x, c) =>
        c.copy(filter = x))
        .text("the fraction of top k-mers to keep, others are removed likely due to contamination")

      opt[String]('p', "pattern").valueName("<pattern>").action((x, c) =>
        c.copy(pattern = x)).text("")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

      opt[Int]('n', "n_partition").action((x, c) =>
        c.copy(n_partition = x))

        .text("paritions for the input, only applicable to local files")

      opt[Int]("n_block").action((x, c) =>
        c.copy(n_block = x))
        .text("")

      help("help").text("prints this usage text")
    }
    parser.parse(args, Config())
  }

  def logInfo(str: String) = {
    println(str)
    logger.info(str)
  }

  def filterCluster(config: Config,sc: SparkContext):RDD[String]={
    val lpaRDD= sc.textFile(config.lpa_input).map {
      line => line.split(",")
    }.map(x=>x.map(_.toInt)).filter(x=>x.length>=config.size)
    logInfo(s"There are ${lpaRDD.count()} clusters whose size>${config.size}")
    val filter_result = lpaRDD.map(_.mkString(","))
    return filter_result
  }

  def rpKmer( sampleRDD:RDD[(String,String,String)],filter_result:RDD[String],
                config: Config,sc: SparkContext):RDD[String] = {
    println("rpKmer is running...")
    val clusterRDD= filter_result
      .map(_.split(",")).map(x => x.map(_.toInt)).
      filter(x=>x.length>=0).
      zipWithIndex().map {
      case (nodes, idx) =>
        nodes.map((_, idx.toInt))
    }.flatMap(x => x).map(x=>(x._1.toString,x._2.toString))//local cluster: (readsID,clusterID)
    val kmerRDD = sampleRDD.map(x=>(x._3,x._1))//local cluster:(readsID,kmerID)
    val resultRDD = clusterRDD.join(kmerRDD)
      .map(x=>x._2)
      .groupByKey()
      .persist(StorageLevel.MEMORY_AND_DISK)//(clusterID,Iterator(kmer1,kmer2...))
    val kmerPair=resultRDD.map{
      case(id,kmer)=> {
        val kmerNumMap = scala.collection.mutable.Map[String, Int]()
        for (i <- 0 until kmer.size) {
          var temp = kmerNumMap.getOrElse(kmer.toSeq(i),0)
          kmerNumMap.put(kmer.toSeq(i),temp+1)
        }//map1(kmer1->3,kmer2->5)
        val kmerNumFilterMap=kmerNumMap.filter(x=>x._2>1).toList.sortBy(x=>(x._2,x._1))//sort from low to high

        val typicalKmer=
          if(kmerNumFilterMap.isEmpty) List("null")
          else {
            val sumKmerSize=kmerNumFilterMap.map(_._2).reduce(_+_)
            var sum=0
            var i = 0
            while (i < kmerNumFilterMap.size && sum<=sumKmerSize/2.0){
              sum+=kmerNumFilterMap(i)._2
              i=i+1
            }
            val medianKmerSize =kmerNumFilterMap(i-1)._2 //median kmer id
            val kmer=kmerNumFilterMap.filter(x=>x._2==medianKmerSize)
              .map(_._1).sortBy(x=>x).take(config.kmercount)
            kmer
          }
        (typicalKmer,id.toLong)
      }
    }.map{
      case(list,id)=>
        val pair=0.until(list.size).map{ i=>(list(i),id)}
        pair
    }.flatMap(x=>x)
    println("kmerPair: ")
    kmerPair.take(30).foreach(println(_))
    val type_result = kmerPair.map{
      case (kmer,clusterID)=>
        kmer+" "+ clusterID
    }//.persist(StorageLevel.MEMORY_AND_DISK)
    resultRDD.unpersist()

    return type_result
  }

  def abunMatrix(sampleRDD:RDD[(String,String,String)],type_result:RDD[String],
                 config: Config, sc: SparkContext):RDD[String] = {
    /*First map representative kmer to originning samples,
    then get the median and construct kmer abundance matrix*/
    println("abunMatrix is running...")
    val clusterRDD=type_result.map{
      lines=>lines.split(" |\n")
    }.map(x=>(x(0),x(1).toLong))//(kmer,cluster)
    val samplerdd = sampleRDD.map(x=>(x._1,x._2))//(kmer,sample)
    println(s"samplecount: ${config.samples}")
    val sampleCount=config.samples

    val result =clusterRDD.filter(x=>x._1!="null")//(kmer,id)
    val nullSize=clusterRDD.count()-result.count()
    logInfo(s"There are ${nullSize} clusters have no typical kmers in total ${result.count()} clusters")
    val clusterJoinSample= result.join(samplerdd)
      .map(x=>((x._1,x._2._1),x._2._2))
      .groupByKey()
      .map(x => (x._1, x._2.toSeq))//(kmer,cluster) + (kmer,sample)=((kmer,cluster),(sample1,sample2....))
      .persist(StorageLevel.MEMORY_AND_DISK)

    val allKmer=clusterJoinSample.map{ //all representative kmer count
      case(kmerCluster,samples)=>
        val sampleNumMap = scala.collection.mutable.Map[Int, Int]()
        val sampleMapArray = new Array[Int](config.samples)
        samples.foreach {
          x =>
            var temp = sampleNumMap.getOrElse(x.toInt, 0)
            sampleNumMap.put(x.toInt, temp + 1)
        }
        sampleNumMap.foreach(x =>sampleMapArray(x._1) = x._2)
        (kmerCluster._2.toInt,sampleMapArray)
    }
    val medianKmer=allKmer.groupByKey().map{//get median representative kmer count for above
      case(cluster,sampleArray)=>
        val clusterCount=sampleArray.size
        val sample_array = new Array[Int](sampleCount+1)
        for(i<-0 until(sampleCount)){
          val cluster_array = new Array[Int](clusterCount)
          for(j<-0 until(clusterCount)){
            cluster_array(j)=sampleArray.toSeq(j)(i)
          }
          val median_temp=cluster_array.sorted
          sample_array(i)=median_temp(clusterCount/2)
          sample_array(sampleCount)=cluster //the last number is clusterID
        }
        sample_array
    }
    val AbunMatrix= medianKmer.sortBy(x=>x.last)
      .map(x=>x.take(sampleCount).map(_.toDouble))
      .map(Vectors.dense(_))
    AbunMatrix.take(10).foreach(println(_))
    val matrix_result = AbunMatrix.map(_.toArray.map(_.toInt).mkString(","))

    matrix_result
  }

  def cosine(matrix_result:RDD[String],config: Config, sc: SparkContext)={
    val Abun= matrix_result
      .map(_.split(","))
      .map{x=>
        val result=for(elem<-x) yield elem.toDouble
        result
      }.zipWithIndex().map(x=>(x._1,x._2.toInt)).repartition(config.n_partition)
    //matrix_result.unpersist()
    val AbunBroad = sc.broadcast(Abun.collect())
    import breeze.linalg._
    import breeze.numerics._
    val cosineSimilarity=Abun.flatMap{
      case(array,id)=>
        val a=DenseVector(array)
        val table=AbunBroad.value
        val buf = new ListBuffer[((Int, Int), Double)]()
        for(i<-0 until table.length){
          if(id<table(i)._2) {
            val b = DenseVector(table(i)._1)
            val similar = (a dot b) / (norm(a) * norm(b))
            if(similar>=config.filter)
            {
              buf += (((id, table(i)._2), similar))
            }
          }
        }
        buf
    }.map(x=>(x._1._1,x._1._2,x._2)).filter(x=>x._1!=x._2)
    cosineSimilarity.take(30).foreach(println(_))
    KmerCounting.delete_hdfs_file(config.output)
    cosineSimilarity.map{
      case (kmer1,kmer2,value)=>
        kmer1.toInt+","+kmer2.toInt+","+value.toDouble
    }.saveAsTextFile(config.output)
  }
   def run(config: Config, spark: SparkSession): Unit = {
      val sc = spark.sparkContext
      val start = System.currentTimeMillis
      logInfo(new java.util.Date(start) + ": Program started ...")

      val sampleRDD = sc.textFile(config.mini_input).map {
        line => line.split("\t| |\n")
      }.map { x => (x(0), x(1), x(2)) }.persist(StorageLevel.MEMORY_AND_DISK)//(kmer,sample,read)
      val filter_result = filterCluster(config,sc)
      val type_result = rpKmer(sampleRDD,filter_result,config,sc)
      val matrix_result = abunMatrix(sampleRDD, type_result, config, sc)
      cosine(matrix_result, config, sc)
      sampleRDD.unpersist()
      val totalTime1 = System.currentTimeMillis
      logInfo("Total process time: %.2f minutes".format((totalTime1 - start).toFloat / 60000))
  }


  override def main(args: Array[String]) {
    val APPNAME = "Spark GlobalClustering"

    val options = parse_command_line(args)
    logger.info(s"called with arguments\n${options.valueTreeString}")

    options match {
      case Some(_) =>
        val config = options.get

        logInfo(s"called with arguments\n${options.valueTreeString}")
        val conf = new SparkConf().setAppName(APPNAME).set("spark.kryoserializer.buffer.max", "512m")
        conf.registerKryoClasses(Array(classOf[DNASeq]))

        val spark = SparkSession
          .builder().config(conf)
          .appName(APPNAME)
          .getOrCreate()

        run(config, spark)
        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        spark.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  } //main
}
