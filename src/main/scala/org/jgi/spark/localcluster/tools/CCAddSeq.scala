/**
  * Created by Lizhen Shi on 6/8/17.
  */
package org.jgi.spark.localcluster.tools

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import org.jgi.spark.localcluster.tools.CCAddSeq.{GlobalAdd, LpaAdd}
import org.jgi.spark.localcluster.{DNASeq, Utils}
import sext._


object CCAddSeq extends App with LazyLogging {

  case class Config(input: String = "", output: String = "", flag:String = "", local_lpa:String = "",
                    sleep: Int = 0, reads_input: String = "", pattern: String = "", format: String = "seq",
                    n_partition: Int = 0, num_output: Int = 18)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("AddSeq") {
      head("AddSeq", Utils.VERSION)

      opt[String]('i', "input").optional().valueName("<file>").action((x, c) =>
        c.copy(input = x)).text("files of graph edges. e.g. output from GraphCC")

      opt[String]("flag").valueName("").action((x, c) =>
        c.copy(flag = x)).
        validate(x =>
          if (Seq("local","global").contains(x.toLowerCase)) success
          else failure("should be one of <local|global>"))
        .text("clustering schema. <local|global>")

      opt[String]("local_lpa").required().valueName("<file>").action((x, c) =>
        c.copy(local_lpa = x)).text("files of graph edges. e.g. output from GraphCC")

      opt[String]("reads").required().valueName("<dir|file>").action((x, c) =>
        c.copy(reads_input = x)).text("a local dir where seq files are located in,  or a local file, or an hdfs file")

      opt[String]('p', "pattern").valueName("<pattern>").action((x, c) =>
        c.copy(pattern = x)).text("if input is a local dir, specify file patterns here. e.g. *.seq, 12??.seq")

      opt[String]("format").valueName("<format>").action((x, c) =>
        c.copy(format = x)).
        validate(x =>
          if (List("seq", "parquet", "base64").contains(x)) success
          else failure("only valid for seq, parquet or base64")
        ).text("input format (seq, parquet or base64)")


      opt[String]('o', "output").required().valueName("<dir>").action((x, c) =>
        c.copy(output = x)).text("output file")

      opt[Int]('n', "n_partition").action((x, c) =>
        c.copy(n_partition = x))
        .text("paritions for the input")

      opt[Int]("num_output").action((x, c) =>
        c.copy(num_output = x))
        .text("number of paritions for the output")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("waiting seconds before stop spark session. For debug purpose, default 0.")

      help("help").text("prints this usage text")

    }
    parser.parse(args, Config())
  }

  def LpaAdd(config: Config, sc: SparkContext): Unit = {
    println("localclustering is running...")
    val start = System.currentTimeMillis
    logger.info(new java.util.Date(start) + ": Program started ...")

    val ccRDD = (if (config.n_partition > 0)
      sc.textFile(config.local_lpa, minPartitions = config.n_partition)
    else
      sc.textFile(config.local_lpa))
      .map(_.split(",")).map(x => x.map(_.toInt)).zipWithIndex().map {
      case (nodes, idx) =>
        nodes.map((_, idx.toInt))
    }.flatMap(x => x)

   /* val seqFiles = Utils.get_files(config.reads_input.trim(), config.pattern.trim())
    logger.debug(seqFiles)*/
    val readsRDD = KmerMapReads2.make_read_id_rdd(config.reads_input, config.format, sc).map(x => (x._1.toInt, x._2))

    val resultRDD = ccRDD.join(readsRDD).map(_._2).map(x => x._2 + "\t" + x._1.toString).coalesce(config.num_output, shuffle = false)
    KmerCounting.delete_hdfs_file(config.output)
    resultRDD.saveAsTextFile(config.output)


    logger.info(s"save results to ${config.output}")

    val totalTime1 = System.currentTimeMillis
    logger.info("Processing time: %.2f minutes".format((totalTime1 - start).toFloat / 60000))

  }

  def GlobalAdd(config: Config, sc: SparkContext): Unit ={
    println("globalclustering is running...")
    val readsRDD =sc.textFile(config.reads_input).map {
      line => line.split("\t|\n")
    }.map { x => (x(0).toInt, x(2)) }//(id,真号)
   // readsRDD.take(3).foreach(println(_))

    val clusterRDD= sc.textFile(config.local_lpa)//载入成行数字的Cluster_Input
      .map(_.split(",")).map(x => x.map(_.toInt)).zipWithIndex().map {
      case (nodes, idx) =>
        nodes.map((_, idx.toInt))
    }.flatMap(x => x).map(x=>(x._2,x._1))//(cid,readsid)

    val reclusterRDD= sc.textFile(config.input)
      .map(_.split(",")).map(x => x.map(_.toInt)).zipWithIndex().map {
      case (nodes, idx) =>
        nodes.map((_, idx.toInt))
    }.flatMap(x => x)//SpaRC聚类后(cid,rid)

    val readsRecluster=reclusterRDD.join(clusterRDD).map(x=>(x._2._2,x._2._1)) //(id,rid)
      .join(readsRDD).map(x=>(x._2._2,x._2._1)) //(id,rid)+(id,真号)=(真号，rid)

    KmerCounting.delete_hdfs_file(config.output)
    readsRecluster.map(x => x._1 + "\t" + x._2.toString).repartition(1).saveAsTextFile(config.output)
  }

  def run(config: Config, sc: SparkContext): Unit = {

    val start = System.currentTimeMillis

    if(config.flag.toLowerCase=="global"){
      println("Global clustering AddSeq is runing...")
      GlobalAdd(config,sc)
    }else {
      println("Local clustering AddSeq is runing...")
      LpaAdd(config, sc)

    }
  }

  override def main(args: Array[String]) {

    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")
        val conf = new SparkConf().setAppName("Spark CCAddSeq")
        conf.registerKryoClasses(Array(classOf[DNASeq]))

        val sc = new SparkContext(conf)
        run(config, sc)
        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  } //main
}
