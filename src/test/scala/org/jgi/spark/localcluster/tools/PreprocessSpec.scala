package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

/**
  * Created by Lizhen Shi on 5/17/17.
  */
class PreprocessSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext
{
  "parse command line" should "be good" in {
    val cfg = Preprocess.parse_command_line("--in data -o result/Preprocess_output".split(" ")).get
    cfg.input should be("data")
  }

  "PreprocessSpec" should "work on the fastq files" in {
    val cfg = Preprocess.parse_command_line(
      ("-i data/samples/sample13.seq,data/samples/sample18.seq -o result/Preprocess_output -n 1 "+
        "--format seq --paired true --n_output_blocks 1"
        ).split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    Preprocess.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

}
