package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

class MinimizerMapReadsSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {


  "Minimizer" should "work on the global files" in {
    val cfg = MinimizerMapReads.parse_command_line(
      ("--flag GLOBAL -i result/Preprocess_output  --kmer result/kmer " +
        "--minimizer result/minimizer -k 31 -m 25 --n_iteration 1").split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    MinimizerMapReads.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }

  "Minimizer" should "work on the local files" in {
    val cfg = MinimizerMapReads.parse_command_line(
      ("--flag LOCAL -i result/Preprocess_output  --kmer result/kmer " +
        " --minimizer result/minimizer -k 31 -m 25 --n_iteration 1").split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    MinimizerMapReads.run(cfg, sc)
    //Thread.sleep(1000 * 10000)
  }
}