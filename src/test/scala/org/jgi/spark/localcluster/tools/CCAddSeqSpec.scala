package org.jgi.spark.localcluster.tools

import com.holdenkarau.spark.testing.SharedSparkContext
import org.apache.spark.SparkConf
import org.scalatest.{FlatSpec, Matchers, _}
import sext._

/**
  * Created by Lizhen Shi on 6/8/17.
  */
class CCAddSeqSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  override def conf: SparkConf = super.conf.set("spark.ui.enabled", "true")


  "parse command line" should "be good" in {
    val cfg = CCAddSeq.parse_command_line("--reads data -p *.seq -i data --local_lpa data -o data".split(" ")).get
    cfg.input should be("data")
    cfg.local_lpa should be("data")
  }

  "CCAddSeqSpec" should "work on the global files" in {
    val cfg = CCAddSeq.parse_command_line(
      "--reads tmp/proprecess_output  -i tmp/graph_lpa3_graphx_weightedge.txt --local_lpa tmp/graph_lpa3_graphx.txt --flag global -o tmp/global_ccaddseq_output".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    CCAddSeq.run(cfg, sc)
  }

  "CCAddSeqSpec" should "work on the local files" in {
    val cfg = CCAddSeq.parse_command_line(
      " --reads data/result/preprocess_output -i data/result/graphlpa3_global_output --local_lpa data/result/graphlpa3_output -o data/result/ccaddseq_output --flag LOCAL --wait 1 --num_output 1 ".split(" ")
        .filter(_.nonEmpty)).get
    println(s"called with arguments\n${cfg.valueTreeString}")

    CCAddSeq.run(cfg, sc)
  }

}
