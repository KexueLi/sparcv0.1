#!/usr/bin/env bash

CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode $deploy_mode --driver-memory $driver_memory --driver-cores $driver_cores \
--num-executors $num_executors --executor-memory $executor_memory --executor-cores $executor_cores \
--conf spark.default.parallelism=$parallelism --conf spark.speculation=true \
--conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=false \
--class org.jgi.spark.localcluster.tools
EOF`

preprocess=$CMD".Preprocess  $TARGET "
preprocess+=" -i $INPUT_DATA -o $RESULT/preprocess_output --paired $paired --format $format --n_output_blocks 100 "
$preprocess > $LOGS/Preprocess.log 2>&1
echo "Preprocess done"
if [ "$kmer"=="true" ]; then
    KmerMapReads=$CMD".KmerMapReads2 $TARGET "
    KmerMapReads+=" --reads $RESULT/preprocess_output -o $RESULT/kmer  -k ${k} --contamination $n_contamination"
    $KmerMapReads > $LOGS/KmerMapRead.log 2>&1
    graph_input=$RESULT/kmer
    echo "KmerMapReads2 done"
else
    MinimizerMapReads=$CMD".MinimizerMapReads $TARGET "
    MinimizerMapReads+=" -i $RESULT/preprocess_output --kmer $RESULT/kmer --minimizer $RESULT/minimizer "
    MinimizerMapReads+=" -k $k -m $m --n_iteration 1 --flag $flag "
    $MinimizerMapReads > $LOGS/MinimizerMapRead.log 2>&1
    graph_input=$RESULT/minimizer
    echo "MinimizerMapReads done"
fi

GraphGen2=$CMD".GraphGen2 $TARGET  -i $graph_input -o $RESULT/graphgen2_output "
GraphGen2+=" --min_shared_kmers  $min_shared_kmer1  --max_degree $max_degree --n_iteration 1 --wait 1 "
$GraphGen2 > $LOGS/GraphGen2.log 2>&1
echo "GraphGen2 done"

GraphLPA3=$CMD".GraphLPA3 $TARGET -i $RESULT/graphgen2_output  -o $RESULT/graphlpa3_output "
GraphLPA3+=" --min_shared_kmers  $min_shared_kmer1  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster"
GraphLPA3+=" --max_iteration $max_iteration  --weight $weight1 --wait  1 --n_output_blocks 1"
$GraphLPA3 > $LOGS/GraphLPA3.log 2>&1
echo "GraphLPA3 done"

if [ "$flag" = "global" ]; then
    GlobalClustering=$CMD".GlobalClustering $TARGET "
    GlobalClustering+="--lpa_input $RESULT/graphlpa3_output --mini_input $RESULT/kmer -o $RESULT/cosine "
    GlobalClustering+="--samples 2 --size 0 --kmercount $kmercount  --filter $filter  --n_partition 1 --n_block 1 --wait 1 "
    $GlobalClustering > $LOGS/GlobalClustering.log 2>&1

    Global_GraphLPA3=$CMD".GraphLPA3 $TARGET -i $RESULT/cosine  -o $RESULT/graphlpa3_global_output "
    Global_GraphLPA3+=" --min_shared_kmers $min_shared_kmer2  --max_shared_kmers $max_shared_kmers --min_reads_per_cluster $min_reads_per_cluster "
    Global_GraphLPA3+=" --max_iteration $max_iteration --weight $weight --wait 1 --n_output_blocks 1"
    $Global_GraphLPA3 > $LOGS/Global_GraphLPA3.log 2>&1
    echo "Global clustering done"
fi

CCAddSeq=$CMD".CCAddSeq $TARGET "
CCAddSeq+=" --reads $RESULT/preprocess_output -i $RESULT/graphlpa3_global_output --local_lpa $RESULT/graphlpa3_output -o $RESULT/ccaddseq_output "
CCAddSeq+=" --flag $flag --wait 1 --num_output 1 "
$CCAddSeq > $LOGS/CCAddSeq.log 2>&1
echo "CCAddSeq done"

Metric=$CMD".Metric $TARGET -l $RESULT/ccaddseq_output -k $INPUT_KEY "
Metric+=" --flag CAMI -o $RESULT/metric_output -n 2 "
#$Metric > $LOGS/Metric.log
