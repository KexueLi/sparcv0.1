#!/usr/bin/env bash
# spawn a small cluster to run spark applications
# by Zhong Wang @ lbl.gov

# some defaults
MASTER="r5.xlarge"
NODE="r5.xlarge"
VOL=32
SIZE=2
PRICE=0.2
KEY=""
JUPYTER=true
#LOGS="s3://wuda-notebook/logs"
#NOTEBOOK_DIR="--notebook-dir,s3://wuda-notebook/jupyter_notebook"
STEPS="file:///home/wuda/run-sparc.json"

# usage message
usage ()
{
echo "<Launch an EMR cluster on AWS --help>
        --master        EMR master node instance type. eg.r5.xlarge
        --node          EMR slave node instance type. eg.r4.2xlarge
        --size          The number of slave node
        --volume        Disk capacity(GB) in every node.
        --price         The max price you'd like to provide for slave node on spot node.
        --release       EMR version eg.emr-5.17.0
        --key           Your private EC2 .pem file to access the master
        --jupyter       We privode two environment<shell|notebook> to run SpaRC. True for install notebook
        --logs          The path you appoint to put EMR bootstrap log.
        --notebook_dir  The path you appoint to mount your S3 file system."
echo "Example: $0 --master r5.xlarge --node r4.2xlarge --size 2 --volume 100\
  --price 0.6 --release emr-5.17.0 --key kexue0510 --jupyter true\
  --logs s3://kexue-notebook/logs --notebook_dir s3://kexue-notebook/notebook "
exit 0
}
if [ $# -lt 4 ]; then
	usage
fi

# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --master)
      shift
      MASTER=$1
      ;;
    --node)
      shift
      NODE=$1
      ;;
    --size)
      shift
      SIZE=$1
      ;;
    --volume)
      shift
      VOL=$1
      ;;
    --price)
      shift
      PRICE=$1
      ;;
    --release)
      shift
      RELEASE=$1
      ;;
    --jupyter)
      shift
      JUPYTER=$1
      ;;
    --key)
      shift
      KEY=$1
      ;;
    --notebook_dir)
      shift
      NOTEBOOK_DIR=$1
      ;;
    --logs)
      shift
      LOGS=$1
      ;;
    --help)
      shift
      usage
      ;;
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done
if [ "$JUPYTER" = true ]; then
echo "Launch a cluster with Jupyter Notebook"
aws emr create-cluster --release-label ${RELEASE} \
  --name "Run-SpaRC-notebook" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia \
  --ec2-attributes KeyName=${KEY},SubnetId=subnet-01a65bbdd29523d1a,InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups "InstanceGroupType=MASTER,InstanceCount=1,InstanceType=${MASTER},\
EbsConfiguration={EbsOptimized=true,EbsBlockDeviceConfigs=[{VolumeSpecification={VolumeType=gp2,SizeInGB=${VOL}}}]}" \
  "InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE}" \
  --region us-east-1 \
  --log-uri ${LOGS} \
  --bootstrap-actions Name="SpaRC-jupyter",Path="s3://kexue-notebook/artifacts/emr-boostrap-kexue.sh",Args=[--notebook-dir,$NOTEBOOK_DIR]
else
aws emr create-cluster --release-label ${RELEASE} \
  --name "Run-SpaRC" --no-visible-to-all-users \
  --applications Name=Hadoop Name=Spark Name=Ganglia \
  --ec2-attributes KeyName=${KEY},SubnetId=subnet-01a65bbdd29523d1a,InstanceProfile=EMR_EC2_DefaultRole \
  --service-role EMR_DefaultRole \
  --instance-groups "InstanceGroupType=MASTER,InstanceCount=1,InstanceType=${MASTER},\
EbsConfiguration={EbsOptimized=true,EbsBlockDeviceConfigs=[{VolumeSpecification={VolumeType=gp2,SizeInGB=${VOL}}}]}" \
  "InstanceGroupType=CORE,InstanceCount=${SIZE},InstanceType=${NODE},BidPrice=${PRICE}" \
  --region us-east-1 \
  --log-uri ${LOGS} \
  --bootstrap-actions Name="SpaRC",Path="s3://wuda-notebook/bash/emr-boostrap-sparc-cached.sh"
 # --steps ${STEPS} \
  #--auto-terminate
fi