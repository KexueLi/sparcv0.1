#!/usr/bin/env bash
#dir
TARGET=$SCRIPTPATH/target/scala-2.11/SpaRC-assembly-0.1.jar #JAR

#cd $(pwd $INPUT)|mkdir logs
#LOGS=$SCRIPTPATH/logs
#INPUT=$SCRIPTPATH/data/samples
#INPUT_DATA=$INPUT/sample13.seq,$INPUT/sample16.seq,$INPUT/sample18.seq,$INPUT/sample20.seq,$INPUT/sample28.seq,$INPUT/sample32.seq,$INPUT/sample34.seq,$INPUT/sample38.seq,$INPUT/sample41.seq
#RESULT=$SCRIPTPATH/result
#INPUT_KEY=data/label100

##spark env options
deploy_mode=client
driver_memory=40G
driver_cores=6
num_executors=100
executor_memory=16G
executor_cores=2
parallelism=100


##performance parameter:

flag=local
kmer=true
paired=false
format=seq
n_contamination=0
min_kmer_count=2
max_kmer_count=200
k=17
m=25
min_shared_kmer1=5
min_shared_kmer2=0
max_shared_kmers=20000
min_reads_per_cluster=2
max_degree=100
max_iteration=10
weight1=none
weight2=edge
filter=0
kmercount=100


