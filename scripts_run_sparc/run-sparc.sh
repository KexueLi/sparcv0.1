#!/usr/bin/env bash

MODE=""

# usage message
usage ()
{
echo "select SpaRC options:
Important options:
        --mode <single|cluster> A platform option is required to run SpaRC.
        -i --inFile  a local dir where <.fastq|.fasta|.seq> default:$.....
        -o --outFile a local dir where put the result default:$......
        --k
        --min_shared_kmers
        --max_degree
        --cosine threhold
        --num_executor
        --num_cores
        --paraliam
Also, for full SpaRC options: --help
"
 exit 0
}
SpaRChelp ()
{
echo "Full options<--help>
        --mode <single|cluster> A platform option is required to run SpaRC.
        -i --inFile  a local dir where <.fastq|.fasta|.seq> default:$.....
        -o --outFile a local dir where put the result default:$......
        --k
        --min_shared_kmers
        --max_degree
        --cosine threhold
        --num_executor
        --num_cores
        --paraliam
"
 exit 0
}

if [ $# -lt 1 ]; then
        usage
fi
# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --mode)
      shift
      MODE=$1
      ;;
    --input)
      shift
      INPUT=$1
      ;;
    --help)
      shift
      SpaRChelp
      ;;
    -*)
      echo "Unknown options: $1"
      usage
      ;;
    *)
      break;
      ;;
    esac
    shift
done
echo "mode: ""$MODE"


##locate the dir
SCRIPTPATH="$( cd "$(dirname "$0")" ;cd ..; pwd)"
DIR="$(dirname  "$INPUT")"
PATH=$SCRIPTPATH:$PATH
PATH=$DIR:$PATH
if (test -f $INPUT) ;then
   INPUT_DATA=`basename $INPUT`
else
   datadir="$(cd $INPUT/..;ls `basename $INPUT`*)"
   for state in $datadir
        do INPUT_DATA+=samples/$state","
done
   INPUT_DATA=${INPUT_DATA::-1}
fi
echo $INPUT_DATA
cd $DIR;
if !(test -d logs) ;then
    mkdir logs
fi
if !(test -d result); then
    mkdir result
fi
LOGS=$DIR/logs
RESULT=$DIR/result
cd $SCRIPTPATH; . $SCRIPTPATH/scripts_run_sparc/config.sh

single(){
##spark env configure:
    echo "loading the single configure"
    SPARK_SUBMIT=$SPARK_HOME/bin/spark-submit
    MASTER=local[$executor_cores]
    INPUT_DATA=$INPUT
}
cluster(){
    echo "loading the cluster configure"
    SPARK_SUBMIT=spark-submit
    MASTER=yarn
    hadoop fs -D dfs.blocksize=33554432 -put $INPUT
    hadoop fs -mkdir result
    RESULT="result"
    #hadoop fa -put ...
}
if [ "$MODE" = "single" ]; then
        single
        . $SCRIPTPATH/scripts_run_sparc/sparc.sh
elif [ "$MODE" = "cluster" ];then
        cluster
        . $SCRIPTPATH/scripts_run_sparc/sparc.sh
        hadoop fs -getmerge $RESULT/ccaddseq_output $INPUT/result/ccaddseq_output
fi