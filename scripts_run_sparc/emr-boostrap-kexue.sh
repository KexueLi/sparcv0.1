#!/bin/bash
set -x -e

# AWS EMR bootstrap script 
SPARC=false
NOTEBOOK_DIR=""
JUPYTER_PORT=8888
JUPYTER_PASSWORD=""
USE_CACHED_DEPS=false
INSTALL_DASK=false

# get input parameters
while [ $# -gt 0 ]; do
    case "$1" in
    --sparc)
      SPARC=true
      ;;
    --port)
      shift
      JUPYTER_PORT=$1
      ;;
    --password)
      shift
      JUPYTER_PASSWORD=$1
      ;;
    --notebook-dir)
      shift
      NOTEBOOK_DIR=$1
      ;;
    --dask)
      INSTALL_DASK=true
      ;;
    -*)
      # do not exit out, just note failure
      error_msg "unrecognized option: $1"
      ;;
    *)
      break;
      ;;
    esac
    shift
done


# check for master node
IS_MASTER=false
if grep isMaster /mnt/var/lib/info/instance.json | grep true;
then
  IS_MASTER=true
fi

# error message
error_msg ()
{
  echo 1>&2 "Error: $1"
}

# some defaults

sudo bash -c 'echo "fs.file-max = 25129162" >> /etc/sysctl.conf'
sudo sysctl -p /etc/sysctl.conf
sudo bash -c 'echo "* soft    nofile          1048576" >> /etc/security/limits.conf'
sudo bash -c 'echo "* hard    nofile          1048576" >> /etc/security/limits.conf'
sudo bash -c 'echo "session    required   pam_limits.so" >> /etc/pam.d/su'

RELEASE=$(cat /etc/system-release)
REL_NUM=$(ruby -e "puts '$RELEASE'.split.last")

sudo yum update -y

if [ true = false ]; then
 sudo yum install -y docker
 sudo service docker start
 sudo usermod -a -G docker hadoop
fi

if [ true = false ]; then
 sudo yum install -y git
 git config --global user.name "Lizhen Shi"
 git config --global user.email "lizhen09.shi@gmail.com"
fi

if [ true = true ]; then
 mkdir -p $HOME/bin
 cat << EOF >> ~/.bashrc 

 export PATH="$HOME/bin:$HOME/local/bin:$HOME/miniconda2/bin:$PATH"
 export LD_LIBRARY_PATH=$HOME/local/lib

 alias l='ls -alrt'
 alias h='history'
 set -o vi
EOF
 source ~/.bashrc
 aws s3 cp s3://lizhen-notebook/zstd  $HOME/bin
 chmod u+x $HOME/bin/zstd
fi


if [ true = false]; then
 wget --quiet https://repo.continuum.io/miniconda/Miniconda2-4.4.10-Linux-x86_64.sh -O ~/miniconda.sh
 /bin/bash ~/miniconda.sh -b -p $HOME/miniconda2
 conda create -c conda-forge -n python27 python=2.7 fastparquet python-snappy dask distributed ipykernel feather-format fbprophet  scikit-learn joblib statsmodels nb_conda zstandard  pysam biopython cython seaborn
else
 rm -fr $HOME/miniconda2
 #aws s3 cp s3://lizhen-notebook/lib/miniconda2.tar.gz /tmp/
 aws s3 cp s3://lizhen-notebook/lib/miniconda2.tar.zst /tmp/
 aws s3 cp s3://lizhen-notebook/bin/local.tgz /tmp/
 cd $HOME 
 tar -I zstd -xf /tmp/miniconda2.tar.zst
 tar xf /tmp/local.tgz
 rm /tmp/miniconda2.tar.*
 rm /tmp/local.tgz
fi

if [ true = true ]; then
 mkdir -p $HOME/local/lib
 aws s3 cp s3://lizhen-notebook/lib/pysparc-0.1-cp27-cp27mu-linux_x86_64.whl  /tmp/
 pip install /tmp/pysparc-0.1-cp27-cp27mu-linux_x86_64.whl
 rm -fr /tmp/pysparc-0.1-cp27-cp27mu-linux_x86_64.whl
fi

if [ true = true ]; then
 aws s3 cp s3://lizhen-notebook/bin/key - | zstd -d -o $HOME/lizhen.pem
 chmod 600 $HOME/lizhen.pem
fi


if [ "$SPARC" = true ]; then
 aws s3 cp s3://lizhen-notebook/lib/LocalCluster-assembly-0.1.jar  $HOME
 aws s3 cp s3://lizhen-notebook/lib/LocalCluster-assembly-0.2.jar  $HOME
fi
if [ false = true ]; then
  curl https://bintray.com/sbt/rpm/rpm | sudo tee /etc/yum.repos.d/bintray-sbt-rpm.repo
  sudo yum install sbt -y
  cd /usr/local/share/
  sudo yum install git-core -y
  curl https://bintray.com/sbt/rpm/rpm | sudo tee /etc/yum.repos.d/bintray-sbt-rpm.repo
  sudo git clone https://zhong_wang:Roger1999@bitbucket.org/LizhenShi/sparc.git/ sparc
  cd sparc
  sudo mv build.sbt.spark2.31.hadoop2.8.4 build.sbt
  sudo sbt assembly
fi

# only run below on master instance
if [ "$IS_MASTER" = true ]; then

sudo mkdir -p /var/log/jupyter
mkdir -p ~/.jupyter
touch ~/.jupyter/jupyter_notebook_config.py

sed -i '/c.NotebookApp.open_browser/d' ~/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.open_browser = True" >> ~/.jupyter/jupyter_notebook_config.py

if [ ! "$JUPYTER_LOCALHOST_ONLY" = true ]; then
sed -i '/c.NotebookApp.ip/d' ~/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.ip='*'" >> ~/.jupyter/jupyter_notebook_config.py
fi

sed -i '/c.NotebookApp.port/d' ~/.jupyter/jupyter_notebook_config.py
echo "c.NotebookApp.port = $JUPYTER_PORT" >> ~/.jupyter/jupyter_notebook_config.py

if [ ! "$JUPYTER_PASSWORD" = "" ]; then
  sed -i '/c.NotebookApp.password/d' ~/.jupyter/jupyter_notebook_config.py
  HASHED_PASSWORD=$(python -c "from notebook.auth import passwd; print(passwd('$JUPYTER_PASSWORD'))")
  echo "c.NotebookApp.password = u'$HASHED_PASSWORD'" >> ~/.jupyter/jupyter_notebook_config.py
else
  sed -i '/c.NotebookApp.token/d' ~/.jupyter/jupyter_notebook_config.py
  echo "c.NotebookApp.token = u''" >> ~/.jupyter/jupyter_notebook_config.py
fi

echo "c.Authenticator.admin_users = {'$JUPYTER_HUB_DEFAULT_USER'}" >> ~/.jupyter/jupyter_notebook_config.py
echo "c.LocalAuthenticator.create_system_users = True" >> ~/.jupyter/jupyter_notebook_config.py



if [ ! "$NOTEBOOK_DIR" = "" ]; then

  NOTEBOOK_DIR="${NOTEBOOK_DIR%/}/" # remove trailing / if exists then add /
  if [[ "$NOTEBOOK_DIR" == s3://* ]]; then
    NOTEBOOK_DIR_S3=true
    if [ true = true ]; then
      BUCKET=$(ruby -e "puts '$NOTEBOOK_DIR'.split('//')[1].split('/')[0]")
      FOLDER=$(ruby -e "puts '$NOTEBOOK_DIR'.split('//')[1].split('/')[1..-1].join('/')")
      if [ "$USE_CACHED_DEPS" != true ]; then
        sudo yum install -y automake fuse fuse-devel libxml2-devel git libcurl-devel jsoncpp-devel
      fi
      cd /mnt
      rm -fr s3fs-fuse
      git clone https://github.com/s3fs-fuse/s3fs-fuse.git
      cd s3fs-fuse/
      ls -alrt
      ./autogen.sh
      ./configure
      make
      sudo make install
      sudo su -c 'echo user_allow_other >> /etc/fuse.conf'
      mkdir -p /mnt/s3fs-cache
      mkdir -p /mnt/$BUCKET
      /usr/local/bin/s3fs -o allow_other -o iam_role=auto -o umask=0 -o url=https://s3.amazonaws.com  -o no_check_certificate -o enable_noobj_cache -o use_cache=/mnt/s3fs-cache $BUCKET /mnt/$BUCKET
      echo "c.NotebookApp.notebook_dir = '/mnt/$BUCKET/$FOLDER'" >> ~/.jupyter/jupyter_notebook_config.py
      echo "c.ContentsManager.checkpoints_kwargs = {'root_dir': '.checkpoints'}" >> ~/.jupyter/jupyter_notebook_config.py
    fi
  else
    echo "c.NotebookApp.notebook_dir = '$NOTEBOOK_DIR'" >> ~/.jupyter/jupyter_notebook_config.py
    echo "c.ContentsManager.checkpoints_kwargs = {'root_dir': '.checkpoints'}" >> ~/.jupyter/jupyter_notebook_config.py
  fi
fi

  cd ~
  nohup jupyter notebook --no-browser &

fi #end of is master

if [ "$INSTALL_DASK" = true ]; then
  if [ "$IS_MASTER" = true ]; then
    dask-scheduler > /var/log/dask-scheduler.log 2>&1 &
  else
    MASTER_KV=$(grep masterHost /emr/instance-controller/lib/info/job-flow-state.txt)
    MASTER_HOST=$(ruby -e "puts '$MASTER_KV'.gsub('\"','').split.last")
    dask-worker $MASTER_HOST:8786 > /var/log/dask-worker.log 2>&1 &
  fi
fi



echo "Bootstrap action finished"

